xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://xmlns.oracle.com/pcbpel/adapter/db/GetPromoCodeBS";
(:: import schema at "../Resources/GetPromoCodeBS.xsd" ::)
declare namespace ns2="http://xmlns.oracle.com/pcbpel/adapter/db/sp/dbReference";
(:: import schema at "../Resources/PromoCodeRBM_sp.xsd" ::)

declare variable $input as element() (:: schema-element(ns1:GetPromoCodeBSInput) ::) external;

declare function local:func($input as element() (:: schema-element(ns1:GetPromoCodeBSInput) ::)) as element() (:: schema-element(ns2:InputParameters) ::) {
    <ns2:InputParameters>
        <ns2:EVENTSOURCE>{fn:data($input/ns1:msisdn)}</ns2:EVENTSOURCE>
    </ns2:InputParameters>
};

local:func($input)