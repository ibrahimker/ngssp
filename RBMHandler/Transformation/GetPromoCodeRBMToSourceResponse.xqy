xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns2="http://xmlns.oracle.com/pcbpel/adapter/db/GetPromoCodeBS";
(:: import schema at "../Resources/GetPromoCodeBS.xsd" ::)
declare namespace ns1="http://xmlns.oracle.com/pcbpel/adapter/db/sp/dbReference";
(:: import schema at "../Resources/PromoCodeRBM_sp.xsd" ::)

declare variable $input as element() (:: schema-element(ns1:OutputParameters) ::) external;

declare function local:func($input as element() (:: schema-element(ns1:OutputParameters) ::)) as element() (:: schema-element(ns2:GetPromoCodeBSOutputCollection) ::) {
    <ns2:GetPromoCodeBSOutputCollection>
    <ns2:GetPromoCodeBSOutput>
              <ns2:package_code>{fn:data($input/ns1:GET_PACKAGE_CODE_SAMU)}</ns2:package_code>
           </ns2:GetPromoCodeBSOutput>
    </ns2:GetPromoCodeBSOutputCollection>
};

local:func($input)