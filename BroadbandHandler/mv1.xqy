xquery version "2004-draft";
(:: pragma bea:global-element-parameter parameter="$evSSPQueryPackageInfo1" element="ns1:evSSPQueryPackageInfo" location="evSSPQueryPackageInfo.xsd" ::)
(:: pragma bea:global-element-return element="ns0:evSSPQueryPackageInfoResp" location="evSSPQueryPackageInfoResp.xsd" ::)

declare namespace ns1 = "com/icare/eai/schema/evSSPQueryPackageInfo";
declare namespace ns0 = "com/icare/eai/schema/evSSPQueryPackageInfoResp";
declare namespace xf = "http://tempuri.org/BroadbandHandler/mv1/";

declare function xf:mv1($evSSPQueryPackageInfo1 as element(ns1:evSSPQueryPackageInfo))
    as element(ns0:evSSPQueryPackageInfoResp) {
        <ns0:evSSPQueryPackageInfoResp>
            <ns0:ServiceNumber>{ data($evSSPQueryPackageInfo1/ns1:ServiceNumber) }</ns0:ServiceNumber>
        </ns0:evSSPQueryPackageInfoResp>
};

declare variable $evSSPQueryPackageInfo1 as element(ns1:evSSPQueryPackageInfo) external;

xf:mv1($evSSPQueryPackageInfo1)