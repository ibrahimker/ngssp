xquery version "2004-draft";
(:: pragma bea:global-element-parameter parameter="$evSSPQueryPackageInfo1" element="ns0:evSSPQueryPackageInfo" location="evSSPQueryPackageInfo.xsd" ::)

declare namespace ns0 = "com/icare/eai/schema/evSSPQueryPackageInfo";
declare namespace xf = "http://tempuri.org/BroadbandHandler/msisdn/";


declare function xf:clientid($evSSPQueryPackageInfo1 as element(ns0:evSSPQueryPackageInfo))
    as xs:string {
        data($evSSPQueryPackageInfo1/ns0:ClientID)
};


declare variable $evSSPQueryPackageInfo1 as element(ns0:evSSPQueryPackageInfo) external;

xf:clientid($evSSPQueryPackageInfo1)