xquery version "2004-draft";
(:: pragma bea:global-element-parameter parameter="$viewAccumulatedResponse1" element="ns2:viewAccumulatedResponse" location="UPCCService.wsdl" ::)
(:: pragma bea:global-element-return element="ns1:viewAccumulatedResponse" location="../../LDAPHandler/SAPCService.wsdl" ::)

declare namespace ns2 = "http://com/nsn/adapter/upcc";
declare namespace ns1 = "http://com/nsn/adapter/sapc";
declare namespace ns0 = "java:com.nsn.adapter.sapc";
declare namespace xf = "http://tempuri.org/BroadbandHandler/UPCC/viewAccumulatedComp/";

declare function xf:viewAccumulatedComp($viewAccumulatedResponse1 as element(ns2:viewAccumulatedResponse))
    as element(ns1:viewAccumulatedResponse) {
        <ns1:viewAccumulatedResponse>
            <ns1:return>
                <ns0:Dn>{ data($viewAccumulatedResponse1/ns2:return/ns0:Dn) }</ns0:Dn>
                <ns0:EPC_AccumulatedData>{ data($viewAccumulatedResponse1/ns2:return/ns0:EPC_AccumulatedData) }</ns0:EPC_AccumulatedData>
                <ns0:EPC_AccumulatedName>{ data($viewAccumulatedResponse1/ns2:return/ns0:EPC_AccumulatedName) }</ns0:EPC_AccumulatedName>
                {
                    for $ObjectClass in $viewAccumulatedResponse1/ns2:return/ns0:ObjectClass
                    return
                        <ns0:ObjectClass>{ data($ObjectClass) }</ns0:ObjectClass>
                }
                <ns0:Result>{ data($viewAccumulatedResponse1/ns2:return/ns0:Result) }</ns0:Result>
            </ns1:return>
        </ns1:viewAccumulatedResponse>
};

declare variable $viewAccumulatedResponse1 as element(ns2:viewAccumulatedResponse) external;

xf:viewAccumulatedComp($viewAccumulatedResponse1)