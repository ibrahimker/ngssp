xquery version "2004-draft";
(:: pragma bea:global-element-parameter parameter="$viewQBAUResponse1" element="ns2:viewQBAUResponse" location="UPCCService.wsdl" ::)
(:: pragma bea:global-element-return element="ns1:viewQBAUResponse" location="SAPCService.wsdl" ::)

declare namespace ns2 = "http://com/nsn/adapter/upcc";
declare namespace ns1 = "http://com/nsn/adapter/sapc";
declare namespace ns0 = "java:com.nsn.adapter.sapc";
declare namespace xf = "http://tempuri.org/BroadbandHandler/UPCC/viewQBAUComp/";

declare function xf:viewQBAUComp($viewQBAUResponse1 as element(ns2:viewQBAUResponse))
    as element(ns1:viewQBAUResponse) {
        <ns1:viewQBAUResponse>
            <ns1:return>
                <ns0:Dn>{ data($viewQBAUResponse1/ns2:return/ns0:Dn) }</ns0:Dn>
                <ns0:EPC_Data>{ data($viewQBAUResponse1/ns2:return/ns0:EPC_Data) }</ns0:EPC_Data>
                <ns0:EPC_LimitName>{ data($viewQBAUResponse1/ns2:return/ns0:EPC_LimitName) }</ns0:EPC_LimitName>
                <ns0:GroupId>{ data($viewQBAUResponse1/ns2:return/ns0:GroupId) }</ns0:GroupId>
                {
                    for $ObjectClass in $viewQBAUResponse1/ns2:return/ns0:ObjectClass
                    return
                        <ns0:ObjectClass>{ data($ObjectClass) }</ns0:ObjectClass>
                }
                <ns0:OwnerId>{ data($viewQBAUResponse1/ns2:return/ns0:OwnerId) }</ns0:OwnerId>
                <ns0:Parent>{ data($viewQBAUResponse1/ns2:return/ns0:Parent) }</ns0:Parent>
                <ns0:Permissions>{ data($viewQBAUResponse1/ns2:return/ns0:Permissions) }</ns0:Permissions>
                <ns0:Result>{ data($viewQBAUResponse1/ns2:return/ns0:Result) }</ns0:Result>
                <ns0:ShareTree>{ data($viewQBAUResponse1/ns2:return/ns0:ShareTree) }</ns0:ShareTree>
            </ns1:return>
        </ns1:viewQBAUResponse>
};

declare variable $viewQBAUResponse1 as element(ns2:viewQBAUResponse) external;

xf:viewQBAUComp($viewQBAUResponse1)