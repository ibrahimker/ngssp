xquery version "2004-draft";
(:: pragma bea:global-element-parameter parameter="$invokeEAICompletionRequest1" element="ns0:invokeEAICompletionRequest" location="EAICompletion.wsdl" ::)
(:: pragma bea:global-element-return element="ns1:SSPMessage" location="EAICompletionRequest.xsd" ::)

declare namespace ns1 = "com/icare/eai/schema/evSSPUpdTransHist";
declare namespace ns0 = "http://www.example.org/EAICompletion/";
declare namespace xf = "http://tempuri.org/EPSHandler/EAICompletionReq/";

declare function xf:EAICompletionReq($invokeEAICompletionRequest1 as element(ns0:invokeEAICompletionRequest))
    as element(ns1:SSPMessage) {
        <ns1:SSPMessage>
            <ns1:SSPUpdTransHist>
                <ns1:TransId>{ data($invokeEAICompletionRequest1/TransId) }</ns1:TransId>
                <ns1:ServiceNumber>{ data($invokeEAICompletionRequest1/ServiceNumber) }</ns1:ServiceNumber>
                <ns1:Source>{ data($invokeEAICompletionRequest1/Source) }</ns1:Source>
                <ns1:OrderType>{ data($invokeEAICompletionRequest1/OrderType) }</ns1:OrderType>
                <ns1:TransStatus>{ data($invokeEAICompletionRequest1/TransStatus) }</ns1:TransStatus>
                <ns1:TransErrorMessage>{ data($invokeEAICompletionRequest1/TransErrorMessage) }</ns1:TransErrorMessage>
            </ns1:SSPUpdTransHist>
        </ns1:SSPMessage>
};

declare variable $invokeEAICompletionRequest1 as element(ns0:invokeEAICompletionRequest) external;

xf:EAICompletionReq($invokeEAICompletionRequest1)