xquery version "2004-draft";
(:: pragma bea:global-element-parameter parameter="$sSPMessage1" element="ns1:SSPMessage" location="EAICompletionResponse.xsd" ::)
(:: pragma bea:global-element-return element="ns0:invokeEAICompletionResponse" location="EAICompletion.wsdl" ::)

declare namespace ns1 = "com/icare/eai/schema/evSSPUpdTransHistResp";
declare namespace ns0 = "http://www.example.org/EAICompletion/";
declare namespace xf = "http://tempuri.org/EPSHandler/EAICompletionResponse/";

declare function xf:EAICompletionResponse($sSPMessage1 as element(ns1:SSPMessage))
    as element(ns0:invokeEAICompletionResponse) {
        <ns0:invokeEAICompletionResponse>
            <TransId>{ data($sSPMessage1/ns1:SSPUpdTransHistResp/ns1:TransId) }</TransId>
            <ServiceNumber>{ data($sSPMessage1/ns1:SSPUpdTransHistResp/ns1:ServiceNumber) }</ServiceNumber>
            <Status>{ data($sSPMessage1/ns1:SSPUpdTransHistResp/ns1:Status) }</Status>
            <ErrorMessage>{ data($sSPMessage1/ns1:SSPUpdTransHistResp/ns1:ErrorMessage) }</ErrorMessage>
        </ns0:invokeEAICompletionResponse>
};

declare variable $sSPMessage1 as element(ns1:SSPMessage) external;

xf:EAICompletionResponse($sSPMessage1)