xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns3="http://indosatooredoo.com/ngssp/schemas/packageregistration";
(:: import schema at "../Schema/CheckGlobalBlackList.xsd" ::)
declare namespace ns2="http://xmlns.oracle.com/pcbpel/adapter/db/GlobalBlackList_DB";
(:: import schema at "../../DB/GlobalBlackList_DB.xsd" ::)

declare namespace ns1="http://www.indosatoredoo.com/xquery";

declare variable $DbOutput as element() (:: schema-element(ns2:GlobalBlackList_DBOutputCollection) ::) external;
declare variable $request as element() (:: schema-element(ns3:SendPackageRegistrationRequest) ::) external;

declare function ns1:check($DbOutput as element() (:: schema-element(ns2:GlobalBlackList_DBOutputCollection) ::)
,$request as element() (:: schema-element(ns3:SendPackageRegistrationRequest) ::)
) as element() (:: schema-element(ns3:SendPackageRegistrationResponse) ::) {
    let $keyword := $request/ns3:keyword/text()
    let $msisdn := $request/ns3:charged_msisdn/text()
    let $shortcode := $request/ns3:shortcode/text()
    
    let $prefix_fail := $DbOutput/ns2:GlobalBlackList_DBOutput[ns2:PARAM_TYPE='PREFIX' and starts-with($msisdn,ns2:PARAM_VALUE) and ($shortcode =  ns2:SHORTCODE or ns2:SHORTCODE='%')]/ns2:NOTIF_FAIL[1]/text()
    let $keyword_fail := $DbOutput/ns2:GlobalBlackList_DBOutput[ns2:PARAM_TYPE='KEYWORD' and $keyword = ns2:PARAM_VALUE and ($shortcode =  ns2:SHORTCODE or ns2:SHORTCODE='%')]/ns2:NOTIF_FAIL[1]/text()
    return
    if (exists($prefix_fail) or $prefix_fail != '') then
    <SendPackageRegistrationResponse><result>{$prefix_fail}</result></SendPackageRegistrationResponse>
    
    else if (exists($keyword_fail) or $keyword_fail != '') then
    <SendPackageRegistrationResponse><result>{$keyword_fail}</result></SendPackageRegistrationResponse>
    
    else
    <SendPackageRegistrationResponse><result>OK-{($keyword,'-',$msisdn,'-',$shortcode,'-',$prefix_fail,'-',$keyword_fail)}</result></SendPackageRegistrationResponse>
  
};

ns1:check($DbOutput,$request)