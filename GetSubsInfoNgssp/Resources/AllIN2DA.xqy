xquery version "2004-draft";
(:: pragma bea:global-element-parameter parameter="$getINAllInfo2Response1" element="ns1:GetINAllInfo2Response" location="../INServiceHandler/INBalance/GetINAllInfo2.wsdl" ::)
(:: pragma bea:global-element-return element="ns0:ListDA" location="DAs.xsd" ::)

declare namespace ns1 = "http://www.example.org/GetINAllInfo2/";
declare namespace ns0 = "http://www.example.org/DAs";
declare namespace xf = "http://tempuri.org/PULLHandler/AllIN2DA/";

declare function xf:AllIN2DA($getINAllInfo2Response1 as element(ns1:GetINAllInfo2Response))
    as element(ns0:ListDA) {
        <ns0:ListDA>
        {
        	for $element1 in $getINAllInfo2Response1/*
        	return
        	if (fn:starts-with(xs:string(fn:node-name($element1)),"da")) then (
            <DA>
              <id>{ fn:substring-after(xs:string(fn:node-name($element1)),"da") }</id>
              <value>  {  fn:tokenize($element1,";")[1] }</value>
              <ExpiryDate>{ fn:tokenize($element1,";")[2] }</ExpiryDate>
            </DA>
            ) else
            ()
        }	
        </ns0:ListDA>
};

declare variable $getINAllInfo2Response1 as element(ns1:GetINAllInfo2Response) external;

xf:AllIN2DA($getINAllInfo2Response1)