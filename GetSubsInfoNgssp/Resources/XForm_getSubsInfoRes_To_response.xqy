xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://www.example.org/GetSubsInfo/";
(:: import schema at "GetSubsInfo.wsdl" ::)

declare variable $getSubsInfoResponse as element() (:: schema-element(ns1:GetSubsInfoResponse) ::) external;

declare function local:func($getSubsInfoResponse as element() (:: schema-element(ns1:GetSubsInfoResponse) ::)) as element() (:: schema-element(ns1:GetSubsInfoResponse) ::) {
    <ns1:GetSubsInfoResponse>
        <ns1:Tid>{fn:data($getSubsInfoResponse/Tid)}</ns1:Tid>
        <ns1:Msisdn>{fn:data($getSubsInfoResponse/Msisdn)}</ns1:Msisdn>
        <ns1:IMSI>{fn:data($getSubsInfoResponse/IMSI)}</ns1:IMSI>
        <ns1:SimType>USIM</ns1:SimType>
        <ns1:Offers>
            {
                for $Offer in $getSubsInfoResponse/Offers/Offer/text()
                return <ns1:Offer>{$Offer}</ns1:Offer>
            }
        </ns1:Offers>
        <ns1:ServiceClass>{fn:data($getSubsInfoResponse/ServiceClass)}</ns1:ServiceClass>
        {
            if ($getSubsInfoResponse/CustBalanceInfo)
            then <ns1:CustBalanceInfo>{fn:data($getSubsInfoResponse/CustBalanceInfo)}</ns1:CustBalanceInfo>
            else ()
        }
        {
            if ($getSubsInfoResponse/CustBillInfo)
            then 
                <ns1:CustBillInfo>
                    <ns1:MSISDN>{fn:data($getSubsInfoResponse/CustBillInfo/MSISDN)}</ns1:MSISDN>
                    <ns1:Payment_Due_Date>{fn:data($getSubsInfoResponse/CustBillInfo/ns1:Payment_Due_Date)}</ns1:Payment_Due_Date>
                    <ns1:Pulsa>{fn:data($getSubsInfoResponse/CustBillInfo/Pulsa)}</ns1:Pulsa>
                    <ns1:Invoice_Date>{fn:data($getSubsInfoResponse/CustBillInfo/Invoice_Date)}</ns1:Invoice_Date>
                    <ns1:Tagihan_SKR>{fn:data($getSubsInfoResponse/CustBillInfo/Tagihan_SKR)}</ns1:Tagihan_SKR>
                    <ns1:Total_Tagihan>{fn:data($getSubsInfoResponse/CustBillInfo/Total_Tagihan)}</ns1:Total_Tagihan>
                    <ns1:SAMU_Flag>{fn:data($getSubsInfoResponse/CustBillInfo/SAMU_Flag)}</ns1:SAMU_Flag>
                    <ns1:Tagihan_Asset>{fn:data($getSubsInfoResponse/CustBillInfo/Tagihan_Asset)}</ns1:Tagihan_Asset>
                    {
                        if ($getSubsInfoResponse/ns1:CustBillInfo/RemainingBalance)
                        then <ns1:RemainingBalance>{fn:data($getSubsInfoResponse/CustBillInfo/RemainingBalance)}</ns1:RemainingBalance>
                        else ()
                    }
                    {
                        if ($getSubsInfoResponse/CustBillInfo/CreditLimit)
                        then <ns1:CreditLimit>{fn:data($getSubsInfoResponse/CustBillInfo/CreditLimit)}</ns1:CreditLimit>
                        else ()
                    }
                </ns1:CustBillInfo>
            else ()
        }
        <ns1:SubsType>{fn:data($getSubsInfoResponse/SubsType)}</ns1:SubsType>
        <ns1:ExpiredDate>{fn:data($getSubsInfoResponse/ExpiredDate)}</ns1:ExpiredDate>
        <ns1:TerminateDate>{fn:data($getSubsInfoResponse/TerminateDate)}</ns1:TerminateDate>
        <ns1:Services>
            {
                for $Service in $getSubsInfoResponse/Services/Service
                return 
                <ns1:Service>
                    <ns1:ServiceType>{fn:data($Service/ServiceType)}</ns1:ServiceType>
                    <ns1:ServiceName>{fn:data($Service/ServiceName)}</ns1:ServiceName>
                    <ns1:ServiceDescription>{fn:data($Service/ServiceDescription)}</ns1:ServiceDescription>
                    <ns1:RegistrationKeyword>{fn:data($Service/RegistrationKeyword)}</ns1:RegistrationKeyword>
                    <ns1:ShortCode>{fn:data($Service/ShortCode)}</ns1:ShortCode>
                    <ns1:PackageCode>{fn:data($Service/PackageCode)}</ns1:PackageCode>
                    <ns1:PackageName>{fn:data($Service/PackageName)}</ns1:PackageName>
                    <ns1:StartDate>{fn:data($Service/StartDate)}</ns1:StartDate>
                    <ns1:EndDate>{fn:data($Service/EndDate)}</ns1:EndDate>
                    <ns1:PackagePeriod>{fn:data($Service/PackagePeriod)}</ns1:PackagePeriod>
                    <ns1:PeriodUnit>{fn:data($Service/PeriodUnit)}</ns1:PeriodUnit>
                    <ns1:Quotas>
                        {
                            for $Quota in $Service/Quotas/Quota
                            return 
                            <ns1:Quota>
                                <ns1:Name>{fn:data($Quota/Name)}</ns1:Name>
                                <ns1:Description>{fn:data($Quota/Description)}</ns1:Description>
                                <ns1:RawInitialQuota>{fn:data($Quota/RawInitialQuota)}</ns1:RawInitialQuota>
                                <ns1:RawAditionalQuota>{fn:data($Quota/RawAditionalQuota)}</ns1:RawAditionalQuota>
                                <ns1:RawUsedQuota>{fn:data($Quota/RawUsedQuota)}</ns1:RawUsedQuota>
                                <ns1:RawRemainingQuota>{fn:data($Quota/RawRemainingQuota)}</ns1:RawRemainingQuota>
                                <ns1:InitialQuota>{fn:data($Quota/InitialQuota)}</ns1:InitialQuota>
                                <ns1:AdditionalQuota>{fn:data($Quota/AdditionalQuota)}</ns1:AdditionalQuota>
                                <ns1:UsedQuota>{fn:data($Quota/UsedQuota)}</ns1:UsedQuota>
                                <ns1:RemainingQuota>{fn:data($Quota/RemainingQuota)}</ns1:RemainingQuota>
                                <ns1:QuotaUnit>{fn:data($Quota/QuotaUnit)}</ns1:QuotaUnit>
                                <ns1:BenefitType>{fn:data($Quota/BenefitType)}</ns1:BenefitType>
                                <ns1:ExpiryDate>{fn:data($Quota/ExpiryDate)}</ns1:ExpiryDate>
                                <ns1:QuotaSource>{fn:data($Quota/QuotaSource)}</ns1:QuotaSource>
                                <ns1:Show>{fn:data($Quota/Show)}</ns1:Show>
                            </ns1:Quota>
                        }
                    </ns1:Quotas>
                </ns1:Service>
            }
        </ns1:Services>
    </ns1:GetSubsInfoResponse>
};

local:func($getSubsInfoResponse)