xquery version "2004-draft";
(:: pragma bea:global-element-parameter parameter="$addQBAU1" element="m:addQBAU" location="SAPCService.wsdl" ::)
(:: pragma bea:global-element-return element="ns1:addGeneric" location="SAPCServiceGeneric.wsdl" ::)

declare namespace m = "http://com/nsn/adapter/sapc";
declare namespace java = "java:com.nsn.adapter.sapc";
declare namespace xf = "http://tempuri.org/LDAPHandler/addQbau/";

declare function xf:addQbau($addQBAU1 as element(m:addQBAU))
    as element(m:addGeneric) {
        <m:addGeneric>
            <m:transId>{ data($addQBAU1/m:transId) }</m:transId>
            <m:packageCode>{ data($addQBAU1/m:packageCode) }</m:packageCode>
            <m:dn>{ data($addQBAU1/m:input/java:Dn) }</m:dn>
            <m:attributesList>
                {
                    for $input0 in $addQBAU1/m:input/java:EPC_Data union $addQBAU1/m:input/java:EPC_LimitName union $addQBAU1/m:input/java:GroupId union $addQBAU1/m:input/java:ObjectClass union $addQBAU1/m:input/java:OwnerId union $addQBAU1/m:input/java:Parent union $addQBAU1/m:input/java:Permissions union $addQBAU1/m:input/java:ShareTree
                    return
                        <java:SAPCAttribute>
                            <java:AttributeName>
                                {
                                    if (fn:local-name($input0) = "EPC_Data") then
                                        ("EPC-Data")
                                    else if (fn:local-name($input0) = "EPC_LimitName") then
                                        ("EPC-LimitName")
                                    else if (fn:local-name($input0) = "GroupId") then
                                        ("groupId")
                                    else if (fn:local-name($input0) = "ObjectClass") then
                                        ("objectClass")
                                    else if (fn:local-name($input0) = "OwnerId") then
                                        ("ownerId")
                                    else if (fn:local-name($input0) = "Parent") then
                                        ("parent")
                                    else if (fn:local-name($input0) = "Permissions") then
                                        ("permissions")
                                    else if (fn:local-name($input0) = "ShareTree") then
                                        ("shareTree")
                                    else
                                         ()
                                }
							</java:AttributeName>
                            <java:AttributeValue>{ data($input0) }</java:AttributeValue>
                        </java:SAPCAttribute>
                }
            </m:attributesList>
        </m:addGeneric>
};

declare variable $addQBAU1 as element(m:addQBAU) external;

xf:addQbau($addQBAU1)