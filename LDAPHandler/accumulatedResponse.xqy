xquery version "2004-draft";
(:: pragma bea:global-element-parameter parameter="$queryAllAttributeGenericResponse1" element="m:queryAllAttributeGenericResponse" location="SAPCServiceGeneric.wsdl" ::)
(:: pragma bea:global-element-return element="m:viewAccumulatedResponse" location="SAPCService.wsdl" ::)

declare namespace m = "http://com/nsn/adapter/sapc";
declare namespace java = "java:com.nsn.adapter.sapc";
declare namespace xf = "http://tempuri.org/LDAPHandler/accumulatedResponse/";

declare function xf:accumulatedResponse($queryAllAttributeGenericResponse1 as element(m:queryAllAttributeGenericResponse), $resultStr as xs:string)
    as element(m:viewAccumulatedResponse) {
    <m:viewAccumulatedResponse>
				<m:return>
				<java:Dn xsi:nil="true"></java:Dn>
				{
				if (not ($resultStr='OK')) then
				(
                            <java:EPC_AccumulatedData xsi:nil="true"></java:EPC_AccumulatedData>,
                            <java:EPC_AccumulatedName xsi:nil="true"></java:EPC_AccumulatedName>
				)
				else
                (
                    let $result :=
                        for $SAPCAttribute in $queryAllAttributeGenericResponse1/m:return/java:SAPCAttribute
                        where data($SAPCAttribute/java:AttributeName) = "EPC-AccumulatedData"
                        return
                            <java:EPC_AccumulatedData>{ data($SAPCAttribute/java:AttributeValue[1]) }</java:EPC_AccumulatedData>
                    return
                    if (count($result) = 0) then
                        <java:EPC_AccumulatedData xsi:nil="true"></java:EPC_AccumulatedData>
                    else 
                        $result[1],
                    let $result :=
                        for $SAPCAttribute in $queryAllAttributeGenericResponse1/m:return/java:SAPCAttribute
                        where data($SAPCAttribute/java:AttributeName) = "EPC-AccumulatedName"
                        return
                            <java:EPC_AccumulatedName>{ data($SAPCAttribute/java:AttributeValue[1]) }</java:EPC_AccumulatedName>
                    return
                    if (count($result) = 0) then
                        <java:EPC_AccumulatedName xsi:nil="true"></java:EPC_AccumulatedName>
                    else 
                        $result[1],
                    let $result :=
                        for $SAPCAttribute in $queryAllAttributeGenericResponse1/m:return/java:SAPCAttribute
                        where data($SAPCAttribute/java:AttributeName) = "objectClass"
                        return
                            <java:ObjectClass>{ data($SAPCAttribute/java:AttributeValue[1]) }</java:ObjectClass>
                    return
                    if (count($result) = 0) then
                        <java:ObjectClass xsi:nil="true"></java:ObjectClass>
                    else 
                        $result[1]
                )
                }
					<java:Result>{data($resultStr)}</java:Result>
        				</m:return>
			</m:viewAccumulatedResponse>
};

declare variable $queryAllAttributeGenericResponse1 as element(m:queryAllAttributeGenericResponse) external;
declare variable $resultStr as xs:string external;

xf:accumulatedResponse($queryAllAttributeGenericResponse1,$resultStr)