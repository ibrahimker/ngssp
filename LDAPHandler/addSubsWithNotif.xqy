xquery version "2004-draft";
(:: pragma bea:global-element-parameter parameter="$addSubscriber1" element="m:addSubscriberWithNotif" location="SAPCService.wsdl" ::)
(:: pragma bea:global-element-return element="m:addGeneric" location="SAPCServiceGeneric.wsdl" ::)

declare namespace m = "http://com/nsn/adapter/sapc";
declare namespace java = "java:com.nsn.adapter.sapc";
declare namespace xf = "http://tempuri.org/LDAPHandler/addSubsWithNotif/";

declare function xf:addSubsWithNotif($addSubscriber1 as element(m:addSubscriberWithNotif))
    as element(m:addGeneric) {
        <m:addGeneric>
            <m:transId>{ data($addSubscriber1/m:transId) }</m:transId>
            <m:packageCode>{ data($addSubscriber1/m:packageCode) }</m:packageCode>
            <m:dn>{ data($addSubscriber1/m:input/java:Dn) }</m:dn>
            <m:attributesList>
                {
                    for $input0 in $addSubscriber1/m:input/java:EPC_BlacklistServices union $addSubscriber1/m:input/java:EPC_GroupIds
                    return
                        <java:SAPCAttribute>
                            <java:AttributeName>
                                {
                                    if (fn:local-name($input0) = "EPC_BlacklistServices") then
                                        ("EPC-BlacklistServices")
                                    else if (fn:local-name($input0) = "EPC_GroupIds") then
                                        ("EPC-GroupIds")
                                    else
                                        ()
                                }
                                </java:AttributeName>
                            <java:AttributeValue>{ data($input0) }</java:AttributeValue>
                        </java:SAPCAttribute>,
                        <java:SAPCAttribute>
                            <java:AttributeName>EPC-NotificationData</java:AttributeName>
                            {
			                    for $input0 in $addSubscriber1/m:input/java:EPC_NotificationData
			                    return
			                            <java:AttributeValue>{ data($input0) }</java:AttributeValue>
                               }
                        </java:SAPCAttribute>,
                    for $input0 in $addSubscriber1/m:input/java:EPC_OperatorSpecificInfo union $addSubscriber1/m:input/java:EPC_SubscribedServices union $addSubscriber1/m:input/java:EPC_SubscriberId union $addSubscriber1/m:input/java:GroupId union $addSubscriber1/m:input/java:ObjectClass union $addSubscriber1/m:input/java:OwnerId union $addSubscriber1/m:input/java:Parent union $addSubscriber1/m:input/java:Permissions union $addSubscriber1/m:input/java:ShareTree
                    return
                        <java:SAPCAttribute>
                            <java:AttributeName>
                                {
									if (fn:local-name($input0) = "EPC_OperatorSpecificInfo") then
                                        ("EPC-OperatorSpecificInfo")
                                    else if (fn:local-name($input0) = "EPC_SubscribedServices") then
                                        ("EPC-SubscribedServices")
                                    else if (fn:local-name($input0) = "EPC_SubscriberId") then
                                        ("EPC-SubscriberId")
                                    else if (fn:local-name($input0) = "GroupId") then
                                        ("groupId")
                                    else if (fn:local-name($input0) = "ObjectClass") then
                                        ("objectClass")
                                    else if (fn:local-name($input0) = "OwnerId") then
                                        ("ownerId")
                                    else if (fn:local-name($input0) = "Parent") then
                                        ("parent")
                                    else if (fn:local-name($input0) = "Permissions") then
                                        ("permissions")
                                    else if (fn:local-name($input0) = "ShareTree") then
                                        ("shareTree")
                                    else
                                        ()
                                }
                                </java:AttributeName>
                            <java:AttributeValue>{ data($input0) }</java:AttributeValue>
                        </java:SAPCAttribute>
                }
            </m:attributesList>
        </m:addGeneric>
};

declare variable $addSubscriber1 as element(m:addSubscriberWithNotif) external;

xf:addSubsWithNotif($addSubscriber1)