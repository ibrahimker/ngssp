xquery version "2004-draft" encoding "Cp1252";
(:: pragma bea:global-element-parameter parameter="$queryAllAttributeGenericResponse1" element="ns1:queryAllAttributeGenericResponse" location="SAPCServiceGeneric.wsdl" ::)
(:: pragma bea:global-element-return element="ns1:viewQBAUResponse" location="SAPCService.wsdl" ::)

declare namespace xf = "http://tempuri.org/LDAPHandler/qbauResponse/";
declare namespace m = "http://com/nsn/adapter/sapc";
declare namespace java = "java:com.nsn.adapter.sapc";

declare function xf:qbauResponse($queryAllAttributeGenericResponse1 as element( m:queryAllAttributeGenericResponse), $resultStr as xs:string)
    as element( m:viewQBAUResponse) {
        <m:viewQBAUResponse>
            <m:return>
            <java:Dn xsi:nil="true"></java:Dn>
				{
					if (not ($resultStr='OK')) then
					(
		                <java:EPC_Data xsi:nil="true"></java:EPC_Data>,
		                <java:EPC_LimitName xsi:nil="true"></java:EPC_LimitName>,
		                <java:GroupId xsi:nil="true"></java:GroupId>,
		                <java:OwnerId xsi:nil="true"></java:OwnerId>,
		                <java:Parent xsi:nil="true"></java:Parent>,
		                <java:Permissions xsi:nil="true"></java:Permissions>
					)
					else
					(
	                    let $result :=
	                        for $SAPCAttribute in $queryAllAttributeGenericResponse1/m:return/java:SAPCAttribute
	                        where data($SAPCAttribute/java:AttributeName) = "EPC-Data"
	                        return
	                            <java:EPC_Data>{ data($SAPCAttribute/java:AttributeValue[1]) }</java:EPC_Data>
	                    return
	                    if (count($result) = 0) then
	                        <java:EPC_Data xsi:nil="true"></java:EPC_Data>
	                    else 
	                        $result[1],
	                    let $result :=
	                        for $SAPCAttribute in $queryAllAttributeGenericResponse1/m:return/java:SAPCAttribute
	                        where data($SAPCAttribute/java:AttributeName) = "EPC-LimitName"
	                        return
	                            <java:EPC_LimitName>{ data($SAPCAttribute/java:AttributeValue[1]) }</java:EPC_LimitName>
	                    return
	                    if (count($result) = 0) then
	                        <java:EPC_LimitName xsi:nil="true"></java:EPC_LimitName>
	                    else 
	                        $result[1],
	                    let $result :=
	                        for $SAPCAttribute in $queryAllAttributeGenericResponse1/m:return/java:SAPCAttribute
	                        where data($SAPCAttribute/java:AttributeName) = "groupId"
	                        return
	                            <java:GroupId>{ data($SAPCAttribute/java:AttributeValue[1]) }</java:GroupId>
	                    return
	                    if (count($result) = 0) then
	                        <java:GroupId xsi:nil="true"></java:GroupId>
	                    else 
	                        $result[1],
	                    let $result :=
		                    for $SAPCAttribute in $queryAllAttributeGenericResponse1/m:return/java:SAPCAttribute,
		                        $AttributeValue in $SAPCAttribute/java:AttributeValue
		                    where data($SAPCAttribute/java:AttributeName) = "objectClass"
		                    return
		                        <java:ObjectClass>{ data($SAPCAttribute/java:AttributeValue[1]) }</java:ObjectClass>
	                    return
	                    if (count($result) = 0) then
	                        <java:ObjectClass xsi:nil="true"></java:ObjectClass>
	                    else 
	                        $result[1],
	                    let $result :=
	                        for $SAPCAttribute in $queryAllAttributeGenericResponse1/m:return/java:SAPCAttribute
	                        where data($SAPCAttribute/java:AttributeName) = "ownerId"
	                        return
	                            <java:OwnerId>{ data($SAPCAttribute/java:AttributeValue[1]) }</java:OwnerId>
	                    return
	                    if (count($result) = 0) then
	                        <java:OwnerId xsi:nil="true"></java:OwnerId>
	                    else 
	                        $result[1],
	                    let $result :=
	                        for $SAPCAttribute in $queryAllAttributeGenericResponse1/m:return/java:SAPCAttribute
	                        where data($SAPCAttribute/java:AttributeName) = "parent"
	                        return
	                            <java:Parent>{ data($SAPCAttribute/java:AttributeValue[1]) }</java:Parent>
	                    return
	                    if (count($result) = 0) then
	                        <java:Parent xsi:nil="true"></java:Parent>
	                    else 
	                        $result[1],
	                    let $result :=
	                        for $SAPCAttribute in $queryAllAttributeGenericResponse1/m:return/java:SAPCAttribute
	                        where data($SAPCAttribute/java:AttributeName) = "permissions"
	                        return
	                            <java:Permissions>{ data($SAPCAttribute/java:AttributeValue[1]) }</java:Permissions>
	                    return
	                    if (count($result) = 0) then
	                        <java:Permissions xsi:nil="true"></java:Permissions>
	                    else 
	                        $result[1]
	                )
                }
				<java:Result>{data($resultStr)}</java:Result>
				{
					if (not ($resultStr='OK')) then
					(
	                            <java:ShareTree></java:ShareTree>
					)
					else
	                (
	                    let $result :=
	                        for $SAPCAttribute in $queryAllAttributeGenericResponse1/m:return/java:SAPCAttribute
	                        where data($SAPCAttribute/java:AttributeName) = "shareTree"
	                        return
	                            <java:ShareTree>{ data($SAPCAttribute/java:AttributeValue[1]) }</java:ShareTree>
	                    return
	                    if (count($result) = 0) then
	                        <java:ShareTree xsi:nil="true"></java:ShareTree>
	                    else 
	                        $result[1]
	                )
                }
            </m:return>
        </m:viewQBAUResponse>
};

declare variable $queryAllAttributeGenericResponse1 as element(m:queryAllAttributeGenericResponse) external;
declare variable $resultStr as xs:string external;

xf:qbauResponse($queryAllAttributeGenericResponse1, $resultStr)