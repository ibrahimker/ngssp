xquery version "2004-draft";
(:: pragma bea:global-element-parameter parameter="$addQualification1" element="ns1:addQualification" location="SAPCService.wsdl" ::)
(:: pragma bea:global-element-return element="ns1:addGeneric" location="SAPCServiceGeneric.wsdl" ::)

declare namespace m = "http://com/nsn/adapter/sapc";
declare namespace java = "java:com.nsn.adapter.sapc";
declare namespace xf = "http://tempuri.org/LDAPHandler/addQualification/";

declare function xf:addQualification($addQualification1 as element(m:addQualification))
    as element(m:addGeneric) {
        <m:addGeneric>
            <m:transId>{ data($addQualification1/m:transId) }</m:transId>
            <m:packageCode>{ data($addQualification1/m:packageCode) }</m:packageCode>
            <m:dn>{ data($addQualification1/m:input/java:Dn) }</m:dn>
            <m:attributesList>
                {
                    for $input0 in $addQualification1/m:input/java:EPC_Name union $addQualification1/m:input/java:EPC_SubscriberQualificationData union $addQualification1/m:input/java:ObjectClass
                    return
                        <java:SAPCAttribute>
                            <java:AttributeName>
                                {
                                    if (fn:local-name($input0) = "EPC_Name") then
                                        ("EPC-Name")
                                    else if (fn:local-name($input0) = "EPC_SubscriberQualificationData") then
                                        ("EPC-SubscriberQualificationData")
                                    else if (fn:local-name($input0) = "GroupId") then
                                        ("groupId")
                                    else if (fn:local-name($input0) = "ObjectClass") then
                                        ("objectClass")
                                    else
                                         ()
                                }
							</java:AttributeName>
                            <java:AttributeValue>{ data($input0) }</java:AttributeValue>
                        </java:SAPCAttribute>
                }
            </m:attributesList>
        </m:addGeneric>
};

declare variable $addQualification1 as element(m:addQualification) external;

xf:addQualification($addQualification1)