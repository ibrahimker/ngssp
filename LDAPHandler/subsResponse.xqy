xquery version "2004-draft";
(:: pragma bea:global-element-parameter parameter="$queryAllAttributeGenericResponse1" element="m:queryAllAttributeGenericResponse" location="SAPCServiceGeneric.wsdl" ::)
(:: pragma bea:global-element-return element="m:viewSubscriberResponse" location="SAPCService.wsdl" ::)

declare namespace java = "java:com.nsn.adapter.sapc";
declare namespace m = "http://com/nsn/adapter/sapc";
declare namespace xf = "http://tempuri.org/LDAPHandler/subsResponse/";

declare function xf:subsResponse($queryAllAttributeGenericResponse1 as element(m:queryAllAttributeGenericResponse), $resultStr as xs:string)
    as element(m:viewSubscriberResponse) {
        <m:viewSubscriberResponse>
            <m:return>
				<java:Dn xsi:nil="true"></java:Dn>
				{
					if (not ($resultStr='OK')) then
					(
                            <java:EPC_BlacklistServices xsi:nil="true"></java:EPC_BlacklistServices>,
                            <java:EPC_GroupIds xsi:nil="true"></java:EPC_GroupIds>,
                            <java:EPC_NotificationData xsi:nil="true"></java:EPC_NotificationData>,
                            <java:EPC_OperatorSpecificInfo xsi:nil="true"></java:EPC_OperatorSpecificInfo>,
                            <java:EPC_SubscribedServices xsi:nil="true"></java:EPC_SubscribedServices>,
                            <java:EPC_SubscriberId xsi:nil="true"></java:EPC_SubscriberId>,
                            <java:GroupId xsi:nil="true"></java:GroupId>,
                        	<java:ObjectClass xsi:nil="true"></java:ObjectClass>,
                            <java:OwnerId xsi:nil="true"></java:OwnerId>,
                            <java:Parent xsi:nil="true"></java:Parent>,
                            <java:Permissions xsi:nil="true"></java:Permissions>
					)
					else
					(
                    let $result :=
                        for $SAPCAttribute in $queryAllAttributeGenericResponse1/m:return/java:SAPCAttribute
                        where data($SAPCAttribute/java:AttributeName) = "EPC-BlacklistServices"
                        return
                            <java:EPC_BlacklistServices>{ data($SAPCAttribute/java:AttributeValue[1]) }</java:EPC_BlacklistServices>
                    return 
                    if (count($result) = 0) then
                        <java:EPC_BlacklistServices xsi:nil="true"></java:EPC_BlacklistServices>
                    else 
                        $result[1],
                    let $result :=
                        for $SAPCAttribute in $queryAllAttributeGenericResponse1/m:return/java:SAPCAttribute
                        where data($SAPCAttribute/java:AttributeName) = "EPC-GroupIds"
                        return
                            <java:EPC_GroupIds>{ data($SAPCAttribute/java:AttributeValue[1]) }</java:EPC_GroupIds>
                    return
                    if (count($result) = 0) then
                        <java:EPC_GroupIds xsi:nil="true"></java:EPC_GroupIds>
                    else 
                        $result[1],
                    let $result :=
                        for $SAPCAttribute in $queryAllAttributeGenericResponse1/m:return/java:SAPCAttribute,$AttributeValue in $SAPCAttribute/java:AttributeValue
                        where data($SAPCAttribute/java:AttributeName) = "EPC-NotificationData"
                        return
                            <java:EPC_NotificationData>{ data($AttributeValue) }</java:EPC_NotificationData>
                    return
                    if (count($result) = 0) then
                        ()
                    else 
                        $result,
                    let $result :=
                        for $SAPCAttribute in $queryAllAttributeGenericResponse1/m:return/java:SAPCAttribute
                        where data($SAPCAttribute/java:AttributeName) = "EPC-OperatorSpecificInfo"
                        return
                            <java:EPC_OperatorSpecificInfo>{ data($SAPCAttribute/java:AttributeValue[1]) }</java:EPC_OperatorSpecificInfo>
                    return
                    if (count($result) = 0) then
                        <java:EPC_OperatorSpecificInfo xsi:nil="true"></java:EPC_OperatorSpecificInfo>
                    else 
                        $result[1],
                    let $result :=
                        for $SAPCAttribute in $queryAllAttributeGenericResponse1/m:return/java:SAPCAttribute
                        where data($SAPCAttribute/java:AttributeName) = "EPC-SubscribedServices"
                        return
                            <java:EPC_SubscribedServices>{ data($SAPCAttribute/java:AttributeValue[1]) }</java:EPC_SubscribedServices>
                    return
                    if (count($result) = 0) then
                        <java:EPC_SubscribedServices xsi:nil="true"></java:EPC_SubscribedServices>
                    else 
                        $result[1],
                    let $result :=
                        for $SAPCAttribute in $queryAllAttributeGenericResponse1/m:return/java:SAPCAttribute
                        where data($SAPCAttribute/java:AttributeName) = "EPC-SubscriberId"
                        return
                            <java:EPC_SubscriberId>{ data($SAPCAttribute/java:AttributeValue[1]) }</java:EPC_SubscriberId>
                    return
                    if (count($result) = 0) then
                        <java:EPC_SubscriberId xsi:nil="true"></java:EPC_SubscriberId>
                    else 
                        $result[1],
                    let $result :=
                        for $SAPCAttribute in $queryAllAttributeGenericResponse1/m:return/java:SAPCAttribute
                        where data($SAPCAttribute/java:AttributeName) = "groupId"
                        return
                            <java:GroupId>{ data($SAPCAttribute/java:AttributeValue[1]) }</java:GroupId>
                    return
                    if (count($result) = 0) then
                        <java:GroupId xsi:nil="true"></java:GroupId>
                    else 
                        $result[1],
                    let $result :=
	                    for $SAPCAttribute in $queryAllAttributeGenericResponse1/m:return/java:SAPCAttribute,
	                        $AttributeValue in $SAPCAttribute/java:AttributeValue
	                    where data($SAPCAttribute/java:AttributeName) = "objectClass"
	                    return
	                        <java:ObjectClass>{ data($SAPCAttribute/java:AttributeValue[1]) }</java:ObjectClass>
                    return
                    if (count($result) = 0) then
                        <java:ObjectClass xsi:nil="true"></java:ObjectClass>
                    else 
                        $result[1],
                    let $result :=
                        for $SAPCAttribute in $queryAllAttributeGenericResponse1/m:return/java:SAPCAttribute
                        where data($SAPCAttribute/java:AttributeName) = "ownerId"
                        return
                            <java:OwnerId>{ data($SAPCAttribute/java:AttributeValue[1]) }</java:OwnerId>
                    return
                    if (count($result) = 0) then
                        <java:OwnerId xsi:nil="true"></java:OwnerId>
                    else 
                        $result[1],
                    let $result :=
                        for $SAPCAttribute in $queryAllAttributeGenericResponse1/m:return/java:SAPCAttribute
                        where data($SAPCAttribute/java:AttributeName) = "parent"
                        return
                            <java:Parent>{ data($SAPCAttribute/java:AttributeValue[1]) }</java:Parent>
                    return
                    if (count($result) = 0) then
                        <java:Parent xsi:nil="true"></java:Parent>
                    else 
                        $result[1],
                    let $result :=
                        for $SAPCAttribute in $queryAllAttributeGenericResponse1/m:return/java:SAPCAttribute
                        where data($SAPCAttribute/java:AttributeName) = "permissions"
                        return
                            <java:Permissions>{ data($SAPCAttribute/java:AttributeValue[1]) }</java:Permissions>
                    return
                    if (count($result) = 0) then
                        <java:Permissions xsi:nil="true"></java:Permissions>
                    else 
                        $result[1]
					)
				}
					<java:Result>{data($resultStr)}</java:Result>
                {
                	if (not ($resultStr='OK')) then
                	(
                            <java:ShareTree xsi:nil="true"></java:ShareTree>
                	)
                	else
                	(
                    let $result :=
                        for $SAPCAttribute in $queryAllAttributeGenericResponse1/m:return/java:SAPCAttribute
                        where data($SAPCAttribute/java:AttributeName) = "shareTree"
                        return
                            <java:ShareTree>{ data($SAPCAttribute/java:AttributeValue[1]) }</java:ShareTree>
                    return
                    if (count($result) = 0) then
                        <java:ShareTree xsi:nil="true"></java:ShareTree>
                    else 
                        $result[1]
                    )
                }
            </m:return>
        </m:viewSubscriberResponse>
};

declare variable $queryAllAttributeGenericResponse1 as element(m:queryAllAttributeGenericResponse) external;
declare variable $resultStr as xs:string external;

xf:subsResponse($queryAllAttributeGenericResponse1, $resultStr)