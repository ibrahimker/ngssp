xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns2="http://xmlns.oracle.com/pcbpel/adapter/db/top/QueryLibraryMainService";
(:: import schema at "../../QuerySubsLibrary/Resources/QueryLibraryMainService_table.xsd" ::)
declare namespace ns1="http://xmlns.oracle.com/pcbpel/adapter/db/top/QueryLibraryMainPostpaid";
(:: import schema at "../../QuerySubsLibrary/Resources/QueryLibraryMainPostpaid_table.xsd" ::)

declare variable $postpaidLibrary as element() (:: schema-element(ns1:LibraryMainServicePostpaidCollection) ::) external;

declare function local:func($postpaidLibrary as element() (:: schema-element(ns1:LibraryMainServicePostpaidCollection) ::)) as element() (:: schema-element(ns2:LibraryMainServiceCollection) ::) {
    <ns2:LibraryMainServiceCollection>
        {
            for $LibraryMainServicePostpaid in $postpaidLibrary/ns1:LibraryMainServicePostpaid
            return 
            <ns2:LibraryMainService>
                <ns2:serviceClassId>{fn:data($LibraryMainServicePostpaid/ns1:promoCode)}</ns2:serviceClassId>
                <ns2:description>{fn:data($LibraryMainServicePostpaid/ns1:description)}</ns2:description>
                <ns2:da>{fn:data($LibraryMainServicePostpaid/ns1:da)}</ns2:da>
                <ns2:accumulator>{fn:data($LibraryMainServicePostpaid/ns1:accumulator)}</ns2:accumulator>
                <ns2:sapc>{fn:data($LibraryMainServicePostpaid/ns1:sapc)}</ns2:sapc>
                <ns2:type>POSTPAID</ns2:type>
                <ns2:libraryMainDaCollection>
                  {
                      for $LibraryMainDa in $LibraryMainServicePostpaid/ns1:libraryMainDaCollection/ns1:LibraryMainDa
                      return 
                      <ns2:LibraryMainDa>
                          <ns2:id>{fn:data($LibraryMainDa/ns1:id)}</ns2:id>
                          <ns2:normValue>{fn:data($LibraryMainDa/ns1:normValue)}</ns2:normValue>
                          <ns2:unit>{fn:data($LibraryMainDa/ns1:unit)}</ns2:unit>
                          <ns2:internalDesc>{fn:data($LibraryMainDa/ns1:internalDesc)}</ns2:internalDesc>
                          <ns2:externalDesc>{fn:data($LibraryMainDa/ns1:externalDesc)}</ns2:externalDesc>
                          <ns2:normUnit>{fn:data($LibraryMainDa/ns1:normUnit)}</ns2:normUnit>
                          <ns2:benefitType>{fn:data($LibraryMainDa/ns1:benefitType)}</ns2:benefitType>
                          <ns2:feature>{fn:data($LibraryMainDa/ns1:feature)}</ns2:feature>
                          <ns2:visibility>{fn:data($LibraryMainDa/ns1:visibility)}</ns2:visibility>
                          <ns2:externalDescEng>{fn:data($LibraryMainDa/ns1:externalDescEng)}</ns2:externalDescEng>
                          <ns2:initialValue>{fn:data($LibraryMainDa/ns1:initialValue)}</ns2:initialValue>
                          <ns2:initialUnit>{fn:data($LibraryMainDa/ns1:initialUnit)}</ns2:initialUnit>
                          <ns2:custom>{fn:data($LibraryMainDa/ns1:custom)}</ns2:custom>
                      </ns2:LibraryMainDa>
                  }
                </ns2:libraryMainDaCollection>
                <ns2:libraryMainAccCollection>
                  {
                      for $LibraryMainAcc in $LibraryMainServicePostpaid/ns1:libraryMainAccCollection/ns1:LibraryMainAcc
                      return 
                      <ns2:LibraryMainAcc>
                          <ns2:id>{fn:data($LibraryMainAcc/ns1:id)}</ns2:id>
                          <ns2:normValue>{fn:data($LibraryMainAcc/ns1:normValue)}</ns2:normValue>
                          <ns2:unit>{fn:data($LibraryMainAcc/ns1:unit)}</ns2:unit>
                          <ns2:internalDesc>{fn:data($LibraryMainAcc/ns1:internalDesc)}</ns2:internalDesc>
                          <ns2:externalDesc>{fn:data($LibraryMainAcc/ns1:externalDesc)}</ns2:externalDesc>
                          <ns2:normUnit>{fn:data($LibraryMainAcc/ns1:normUnit)}</ns2:normUnit>
                          <ns2:benefitType>{fn:data($LibraryMainAcc/ns1:benefitType)}</ns2:benefitType>
                          <ns2:feature>{fn:data($LibraryMainAcc/ns1:feature)}</ns2:feature>
                          <ns2:visibility>{fn:data($LibraryMainAcc/ns1:visibility)}</ns2:visibility>
                          <ns2:externalDescEng>{fn:data($LibraryMainAcc/ns1:externalDescEng)}</ns2:externalDescEng>
                          <ns2:initialValue>{fn:data($LibraryMainAcc/ns1:initialValue)}</ns2:initialValue>
                          <ns2:initialUnit>{fn:data($LibraryMainAcc/ns1:initialUnit)}</ns2:initialUnit>
                          <ns2:formula>{fn:data($LibraryMainAcc/ns1:formula)}</ns2:formula>
                      </ns2:LibraryMainAcc>
                  }
                </ns2:libraryMainAccCollection>
                </ns2:LibraryMainService>
        }
    </ns2:LibraryMainServiceCollection>
};

local:func($postpaidLibrary)