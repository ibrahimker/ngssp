xquery version "2004-draft";
(:: pragma bea:global-element-parameter parameter="$getSubsInfoResponse1" element="ns1:GetSubsInfoResponse" location="GetSubsInfo.wsdl" ::)
(:: pragma bea:global-element-return element="ns0:GetSubsInfoResponse" location="GetSubsInfoIndVsn.xsd" ::)

declare namespace ns1 = "http://www.example.org/GetSubsInfo/";
declare namespace ns0 = "http://www.example.org/GetSubsInfoIndVsn/";
declare namespace xf = "http://tempuri.org/PULLHandler/SubInfoIndVsn/";

declare function xf:SubInfoIndVsn($getSubsInfoResponse1 as element(ns1:GetSubsInfoResponse))
    as element(ns0:GetSubsInfoResponse) {
        <ns0:GetSubsInfoResponse>
            <ns0:Tid>{ data($getSubsInfoResponse1/Tid) }</ns0:Tid>
            <ns0:Msisdn>{ data($getSubsInfoResponse1/Msisdn) }</ns0:Msisdn>
            {
                let $Services := $getSubsInfoResponse1/Services
                return
                    <ns0:Services>
                        {
                            for $Service in $Services/Service
                            return
                                <ns0:Service>
                                    <ns0:ServiceType>{ data($Service/ServiceType) }</ns0:ServiceType>
                                    <ns0:ServiceName>{ data($Service/ServiceName) }</ns0:ServiceName>
                                    <ns0:ServiceDescription>{ data($Service/ServiceDescription) }</ns0:ServiceDescription>
                                    <ns0:StartDate>{ data($Service/StartDate) }</ns0:StartDate>
                                    <ns0:EndDate>{ data($Service/EndDate) }</ns0:EndDate>
                                    {
                                        let $Quotas := $Service/Quotas
                                        return
                                            <ns0:Quotas>
                                                {
                                                    for $Quota in $Quotas/Quota
                                                    return
                                                        <ns0:Quota>
                                                            <ns0:Name>{ data($Quota/Name) }</ns0:Name>
                                                            <ns0:Description>{ data($Quota/Description) }</ns0:Description>
                                                            <ns0:InitialQuota>{ data($Quota/InitialQuota) }</ns0:InitialQuota>
                                                            <ns0:AdditionalQuota>{ data($Quota/AdditionalQuota) }</ns0:AdditionalQuota>
                                                            <ns0:UsedQuota>{ data($Quota/UsedQuota) }</ns0:UsedQuota>
                                                            <ns0:RemainingQuota>{ data($Quota/RemainingQuota) }</ns0:RemainingQuota>
                                                            <ns0:QuotaUnit>{ data($Quota/QuotaUnit) }</ns0:QuotaUnit>
                                                            <ns0:BenefitType>{ data($Quota/BenefitType) }</ns0:BenefitType>
                                                            <ns0:ExpiryDate>{ data($Quota/ExpiryDate) }</ns0:ExpiryDate>
                                                            <ns0:QuotaSource>{ data($Quota/QuotaSource) }</ns0:QuotaSource>
                                                            <ns0:Show>{ data($Quota/Show) }</ns0:Show>
                                                        </ns0:Quota>
                                                }
                                            </ns0:Quotas>
                                    }
                                    <ns0:RegistrationKeyword>{ data($Service/RegistrationKeyword) }</ns0:RegistrationKeyword>
                                    <ns0:ShortCode>{ data($Service/ShortCode) }</ns0:ShortCode>
                                    <ns0:PackageCode>{ data($Service/PackageCode) }</ns0:PackageCode>
                                    <ns0:PackageName>{ data($Service/PackageName) }</ns0:PackageName>
                                    <ns0:PackagePeriod>{ data($Service/PackagePeriod) }</ns0:PackagePeriod>
                                    <ns0:PeriodUnit>{ data($Service/PeriodUnit) }</ns0:PeriodUnit>
                                </ns0:Service>
                        }
                    </ns0:Services>
            }
        </ns0:GetSubsInfoResponse>
};

declare variable $getSubsInfoResponse1 as element(ns1:GetSubsInfoResponse) external;

xf:SubInfoIndVsn($getSubsInfoResponse1)