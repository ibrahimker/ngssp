xquery version "2004-draft" encoding "Cp1252";
(:: pragma  parameter="$anyType1" type="xs:anyType" ::)
(:: pragma  type="xs:anyType" ::)

declare namespace xf = "http://tempuri.org/PULLHandler/RemoveNamespace/";

declare function xf:RemoveNamespace($e as element())as element()
 {element { xs:QName(local-name($e)) }
 { for $child in $e/(@*,node())
 return
 if ($child instance of element())
 then xf:RemoveNamespace($child)
 else $child
 }
 };

declare variable $e as element() external;
 xf:RemoveNamespace($e)