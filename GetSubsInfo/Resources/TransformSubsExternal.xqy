xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare variable $ServicesInput as element(Services) external;
declare variable $ServiceFilter as xs:string external;

declare function local:func($ServicesInput as element(Services), 
                            $ServiceFilter as xs:string) 
                            as element(Services) {
    <Services>
      {
        for $svc in $ServicesInput/Service[ServiceType!='Main Package' and ($ServiceFilter='' or contains($ServiceFilter, concat(';',ServiceName,';')))]
        return
          <Service>
            <ServiceName>{data($svc/ServiceName)}</ServiceName>
            <PackageCode>{data($svc/PackageCode)}</PackageCode>
            <PackageName>{data($svc/PackageName)}</PackageName>
            <StartDate>{data($svc/StartDate)}</StartDate>
            <EndDate>{data($svc/EndDate)}</EndDate>
            <PackagePeriod>{data($svc/PackagePeriod)}</PackagePeriod>
            <PeriodUnit>{data($svc/PeriodUnit)}</PeriodUnit>
          </Service>
      }
    </Services>
};

local:func($ServicesInput, $ServiceFilter)