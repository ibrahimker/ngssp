xquery version "2004-draft";
(:: pragma bea:global-element-parameter parameter="$sendSMSRequest1" element="ns1:SendSMSRequest" location="SendSMS.wsdl" ::)
(:: pragma bea:global-element-return element="ns0:invokeEAICompletionRequest" location="../EPSHandler/EAICompletion.wsdl" ::)

declare namespace ns1 = "http://www.example.org/SendSMS/";
declare namespace ns0 = "http://www.example.org/EAICompletion/";
declare namespace xf = "http://tempuri.org/OutgoingSMSHandler/EAICompletionRequest/";

declare function xf:EAICompletionRequest($sendSMSRequest1 as element(ns1:SendSMSRequest))
    as element(ns0:invokeEAICompletionRequest) {
        <ns0:invokeEAICompletionRequest>
            <TransId>{ data($sendSMSRequest1/TransId) }</TransId>
            <ServiceNumber>{ data($sendSMSRequest1/destNumber) }</ServiceNumber>
            <Source>{ data($sendSMSRequest1/Source) }</Source>
            <OrderType>{ data($sendSMSRequest1/OrderType) }</OrderType>
            <TransStatus>{ data($sendSMSRequest1/TransStatus) }</TransStatus>
            <TransErrorMessage>{ data($sendSMSRequest1/TransErrorMessage) }</TransErrorMessage>
        </ns0:invokeEAICompletionRequest>
};

declare variable $sendSMSRequest1 as element(ns1:SendSMSRequest) external;

xf:EAICompletionRequest($sendSMSRequest1)