xquery version "2004-draft";
(:: pragma bea:global-element-return element="ns0:resMSISDN" location="CPIDgen.wsdl" ::)

declare namespace ns0 = "http://xmlns.oracle.com/CPIDgen";
declare namespace xf = "http://tempuri.org/SasnHandler/CGmsisdn/";

declare function xf:CGmsisdn($out as xs:string)
    as element(ns0:resMSISDN) {
        <ns0:resMSISDN>
            <ns0:out>{ $out }</ns0:out>
        </ns0:resMSISDN>
};

declare variable $out as xs:string external;

xf:CGmsisdn($out)