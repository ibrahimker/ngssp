xquery version "2004-draft";
(:: pragma bea:global-element-return element="ns0:resCPID" location="CPIDgen.wsdl" ::)

declare namespace ns0 = "http://xmlns.oracle.com/CPIDgen";
declare namespace xf = "http://tempuri.org/SasnHandler/CGcpid/";

declare function xf:CGcpid($out as xs:string)
    as element(ns0:resCPID) {
        <ns0:resCPID>
            <ns0:out>{ $out }</ns0:out>
        </ns0:resCPID>
};

declare variable $out as xs:string external;

xf:CGcpid($out)