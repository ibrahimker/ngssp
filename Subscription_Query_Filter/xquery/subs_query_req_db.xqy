xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://www.oracle.com/subscription/data";
(:: import schema at "../xsd/ngssp_subscription.xsd" ::)
declare namespace ns2="http://xmlns.oracle.com/pcbpel/adapter/db/top/QueryToDB";
(:: import schema at "../Resources/QueryToDB_table.xsd" ::)

declare variable $query_req as element() (:: schema-element(ns1:ngssp_subscription_query_filter_req) ::) external;

declare function local:func($query_req as element() (:: schema-element(ns1:ngssp_subscription_query_filter_req) ::)) as element() (:: schema-element(ns2:QueryToDBSelectInputParameters) ::) {
    <ns2:QueryToDBSelectInputParameters>
        
        <ns2:msisdn>{fn:data($query_req/ns1:msisdn)}</ns2:msisdn>
        <ns2:param1>{$query_req/ns1:status[1]/text()}</ns2:param1>
        <ns2:param2>{$query_req/ns1:status[2]/text()}</ns2:param2>
        <ns2:param3>{$query_req/ns1:status[3]/text()}</ns2:param3>
        <ns2:param4>{$query_req/ns1:status[4]/text()}</ns2:param4>
        <ns2:param5>{$query_req/ns1:status[5]/text()}</ns2:param5>
        <ns2:param6>{$query_req/ns1:status[6]/text()}</ns2:param6>
        <ns2:param7>{$query_req/ns1:status[7]/text()}</ns2:param7>
        <ns2:param8>{$query_req/ns1:status[8]/text()}</ns2:param8>
        <ns2:param9>{$query_req/ns1:status[9]/text()}</ns2:param9>
        <ns2:param10>{$query_req/ns1:status[10]/text()}</ns2:param10>
        
    </ns2:QueryToDBSelectInputParameters>
};

local:func($query_req)