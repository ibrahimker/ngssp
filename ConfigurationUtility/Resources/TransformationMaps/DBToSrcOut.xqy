xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns2="http://indosatooredoo.com/ngssp/schemas/ConfigurationUtility";
(:: import schema at "../ConfigurationUtility.xsd" ::)
declare namespace ns1="http://xmlns.oracle.com/pcbpel/adapter/db/top/ConfigurationUtilityBS";
(:: import schema at "../ConfigurationUtilityBS_table.xsd" ::)

declare variable $DBOutput as element() (:: schema-element(ns1:SspConfigCollection) ::) external;

declare function local:func($DBOutput as element() (:: schema-element(ns1:SspConfigCollection) ::)) as element() (:: schema-element(ns2:NGsspConfigCollection) ::) {
    <ns2:NGsspConfigCollection>
        {
            for $SspConfig in $DBOutput/ns1:SspConfig
            return 
            <ns2:NgsspConfig>
                <ns2:paramName>{fn:data($SspConfig/ns1:paramName)}</ns2:paramName>
                {
                    if ($SspConfig/ns1:paramValue)
                    then <ns2:paramValue>{fn:data($SspConfig/ns1:paramValue)}</ns2:paramValue>
                    else ()
                }</ns2:NgsspConfig>
        }
    </ns2:NGsspConfigCollection>
};

local:func($DBOutput)