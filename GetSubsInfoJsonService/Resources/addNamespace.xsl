<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
      <xsl:template match="node()|@*">
          <xsl:copy>
              <xsl:apply-templates select="node()|@*" />
          </xsl:copy>
      </xsl:template>
      <xsl:template match="*">
          <xsl:element name="get:{local-name()}" namespace="http://www.example.org/GetSubsInfo/">
              <xsl:apply-templates select="node()|@*"/>
          </xsl:element>
    </xsl:template>
</xsl:stylesheet>