<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ns1="http://www.output.org"
                xmlns:xsd="http://www.w3.org/2001/XMLSchema"
                xmlns:ns0="http://www.input.org"
                exclude-result-prefixes="xsl xsd ns0 ns1">
  <xsl:template match="/">
    <ns1:output>
      <ns1:durationValue>
        <xsl:value-of select="(xsd:dateTime(/ns0:input/ns0:date1) - xsd:dateTime(/ns0:input/ns0:date2))"/>
      </ns1:durationValue>
    </ns1:output>
  </xsl:template>
</xsl:stylesheet>